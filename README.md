# **Load Balancing**
<br/>

![this is image](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUIAAACcCAMAAAA9MFJFAAABL1BMVEX///8JZ5Z6uvkAAAD8/PzHx8fr6+va2trX19dMTEwAY5TGxsbd3d3Dw8MAXpHv7+/Pz8+3t7dKi66goKDZ5+/Q4ep/f3+vr6+iwtQXFxceHh6ZmZmnp6dGRka8vLzR0dGMjIxBQUGNjY1sbGx0dHRiYmJSUlIzMzObm5tlZWWEhITx9vl5eXk6Ojrp8/2AvfkAVoyurrbU6PvLzNiWyPmQxfmIrMSur7/f3+bM3/O/z+FMfrWhorc7gKev1Prs9P0oKChmmLbA3fqhuNWSob+0zOajo7GLlqipyOivt8FzhJ07X46Elbh5pNBrlMGFjKdfjcBLZ4tvep09b6hud4Zbb5eNkayGhpjDxdCUqspUhbtLXIZwhaxlbYe0ztyWt8x3o76Jm6MARWYATIeNstih17rhAAAQ50lEQVR4nO2dC1vayBqAp83kIklI0gBpm5BAuAjBK1S5qLVqpVpXa9vVw3b3aM85/v/fcGYSRAiJJgSl1Lzu02cJMImvM5nrNwEgJuapoEolftbXMOdoCuChKsNKRoS6YTFMRs9ps76oOUMrKbKcUSxgkUpJqmqkbumzvqb5QuVAySRJ0QRKmSpQkmZwTEmY9VXNFTBnGVBVBR5QEiBzrEFRcUGOiYl5rlCkLCssQ8FZX8icIrCKopMkq+uKws76YuYQSEsa8udg0Mqsr2f+IBWJd/SxpMFxXKwwNLKIIVmWFGkuVjgJGskje4Zh2AJjhROgGSyPIDkuVjghssizLLJIxwonRRaRP13HCsXjZvNYnvUFzR8oF/I4F3JMs9mqtQ4+NuNhmgfQmNHXKBcigRLbazbSqVbnsNPYbs3myuYGkSjQw69xLkTozdQKtVJvd9IUtR3nw/spECMSNTsX6l0KUGmkMCVAgWnO7up+KaSMJ1aJQBQGxVkTnUwIqPZKvd5GRTlNr8fZ0MYk3rz1BCt8M6h4ZcZAfZNuo3PYriMAqDXIT/Hd0IYjOM/jJkF80O7GtGRSEg1mvdU5Otxv7+/iYYfjz42nushfG5pgvA5zxFtteFBQFrWCpXUPDtKpo6OjDoQCrZzECm18FObk0VFVmRRJXVL+ON0Fu0c3p2d/ceLnk7gg2/godCPbA13il7MUBNTZ2fmZqH/9Fo9d2wRWqLPI4fo/Zxx1fP5j64eufo0bNQ7BFSoSsvjprKdvbX3//l3988tjX9q8EFghL2GFzPanz9+/n5yc/H0ZF+M+ARWqqDbBU08G1/v87eLi2+VobcwoUBBuW0fPzW1AhbTJ2hMnBieRyJbLkqQaJYkUAQUBpEDmmTkMqBBAXdEUnhQVr4+XkDSSV3XVEsumXvVurP+2BFWIEDh0P/TsF1choHVWzeRMXhKUyvSubi4IodAfo6gt6XxOUi1WETTTiJ7iPDEVhUCgUFGHQBBQXYI6f1NIcY6YjsJnTawwMrHCyMQKIxMrjEysMDKxwsjECiMTK4xMrDAykRVCH6ZzefNArDAyIRVCinKNt8LBP4MjsUJ/KFHRNE2RSPrOEBTcQBAr9AYamuJEnrC6JA2OmrIbUwCxwnEgp2tSP3CHFem7tdacNPZZnY8VeqAo7JBAbkShux6ZmsJH+jOMXBvEt6JoZwuoUCNJJ3ZHZEZX/LsVoishySkpzNlnHjk0nufDw5N3/y8oAC5bBSxQnfSSgyrEy9VJ0RgLmnApBOxSIcdNSaFFZZaot5yao+SMbFUEVeWyU5gdRKUEaBmRy5lCLoMUFkERqJaYE9QMp2YUIWNSJnrLCppeYIWoCPM8Pxa6M6oQcPlsNl+cUo1s0QqnZJiqplZBAVimmZPUKSSLFHImsBilYJKUBmBeKQmSkjMpJSdbwFQ5wbRMrRo4vaALQpzl6k7oDk37KlTyCwgKTEVhhpE4pcjlOLYAitBSdZRzppCspEPKglaGtCqKgRQWgKkUOMuUTL1iAlMzyJxkiMXA6fkppEdfyigX8jwO3aGb283m9r9uFbkUyrZCbjoKRYETOIPWVYEEJBChokGefvhrD0HJMmOoFCfrlKShG4MmS0BTSEPQJMYABtQkoGgC+XBCffwUaosjUdu3uZDrbXd2Qfrw4GN/UY3rXpjJIoN5VCUDhrCeyf5KNOHzd10m8kMKZJFF90KdbTbTqVS6c5hu9Xr2G65cKOFcWKawwkWCIKrKM5hTpgmN9EK0kIHlgcR+DN56D9Zw6E46VROO7Xw40rRGpbeQXchLTo0MpQJKY1H+3ZfY0MS9lPsfcxo1fBeCVrpeb6fSlEB38RuUSTMIGsNAQatU8zlVHzRURfMNSsX6xbZyeP3qQV4HTy1gLtQMFpVjpQdbdRy6k0p1Gsy6vVxd1DCVAkLR8vksqt3y+fLQDZbSyiipkveCpmDs1tvtem3y749S20kmHiS5E/h899wLl4fvhQbDiGS3cXC4jwyuoCON3nDcCYeqkSUN3wizuELJ5qnhtKC0ZBfpiYYma/sbL23e1afT49tLvghAci9oeoFrZFMjmfVO5+gQgUN3uOMLWyGuRYSl8vLyolrE9hzyYxt6GebiREW6vfpywLuVsN/2oBZEICIR9GR+Ct3BtCSbW8p1/7pJwcObm4N0imLW7dAdpya2eyxMNm9nQZQRs8tLHmlSWsku0pTHe368fznMaj3EV31IBVWYDphg8LgTUST5P05rALZPT89POfbrha3ibogaMlquWCiVqoUl1U8T1DP4Hhu4SI8anIrDGSrEfDrrCLB1dvbjnJW+OlETo9MlASZOIK1mCeKtRQ59yu8L7ZduViOX5XsUJhKPqlDSeZLU/zkzuI8/tra2lNzfdoUM/bg3OUErjBRp0zvTrqyOKXz5PuAv5ouPQlQJJ1/tPapCXcEbJq3/c9z9jjj5899O72QyhfiLfOYDQWSdIi0Tng7dxdgmalH2VpjY2byqQfAq8YgKeQkZZJne5T925M63fotmYoX4u5yKmowfMqJgEONtq93dXY9MGD0beipM9n1dJx9PIU/yvB13wnza/ni53bhteHq5CqoQQymoSL/Fje/R66i/W13d8DL4ciNiG9tHYa1zhd5MP55C2J+9YxjNVeQEzo0QchJUEK0PuDM5PLBT98yADhErFJ974c5aGtQeUyHe/VHSFHQ/dPXTBFNxY1JhB/45p0MuDg7Ad/4GX+5Hy4Y+CpPXoPYKDr05fYXAXsowtn/mNCZBadRSrFoKd/vXgdCrKh4qyu+jZES/6gSCn8ka2Ek8qkIvxiZBwyuEOk3d5e36/ruNjfsyIWZ1f/Ir9lCI2jPJFLhKJtOpX0AhEATRmHwGb/chebe8h7UJTzGmMPHi59XVVb/vnNh5kZixQqVaKjIhFcLBoOyKdyXsWZpXN/bDSeyfJTXIeY6rxCvn3mq3ZxI7tdROcpYKAZPPLuQLMOT0U8lyqnkY3GA/K4Y6S8Y+i6MwsZdO/3SyHbK3s3N926xOoVolOUuFk02CopZ1Dv96++EMoso51KUSBP5Tpfr1L+JqLZlcuwI/1xKJtT2QRq+Sa4kOgPaIYmCF3HQVVuwBV7skU8ERFlCTJieAoDfCAau1MGfBDXiLshX2x6SvXr9+DXfX8JG1FEg7oOM/UYZMXAnBkhWney+07ElQFkDA3j8p4wV5b1PGk+PwZ/mPXYyHfgWnOZ28GjpUQ4f+GzTB7HRzIY8LcgnH1Aq855yMNzgXlkUQXmE7xElEnAsXyOFceL25uVmr9XMh3LS5rgFo58LjgOlK0RXegaqRJVSd6N5dZ38oWyAIX5BDdfYELHDsXohy32YykUj+RLkR8yLdr0+e7F4oWM4yBxJvGi7QprlUMi1NCLdar7rg9O32wxrcCHOa4rK9yMOpkZN7V1ebTqumBq7wpGdt57ZG3nnaGhnQOjao4ElQSV3OZjOVaja/EGotCLydk6qFbNS8bIc5Tf+axtqFO05Wvhq0CxNP2y68TSafzWcqg0nQheXJljDc3zceY6Khw/HeSXLv+noT4HbhDtKXeOreid0REQrlbDYvFxbuJkEn3CbkroP3kMzVd+8mG7/27iOj5mHqxWyGGZya2A6W4Bby2Vs8J0EDUX+/sbq68b4+PvM0ysTj/35zJzXwOlmDOy8eWyGkGc4VunM3vkrpFWupWCxmTI2MsqLLmQt0T5y8Gzkw+VCN33jhKwA2Qerx5k4cKEWWJEnTlLvBUdck6N3h4KkCU5U8bp21fpneWLGHv+pwaCh7NVQ9Moq3wp29/+GVSI86ao3aytpgBZN+t97De/IpXLswj9q8H6ru7cghXkyzOjwc019esxFp3NpT4Rp+Az6uQkbWbp8Zw4rDj0mYhkKwbPeUxidCa2Ouaisru6GSHsNTYWIzZb/58/EKsibhJf+2wdHQnYFCYP/AfgBeGIWkvQaRWHyiBbE+A//JF3tXKbjziApJ0jDs0B36vod1QPseGGb2Tqoie9UKQSwH/1I0fBeE4OH/4ZfTVoiXq7OiOP7ImAgFmZFxAcar2nli4cmC9ma1LEkTWRw14fHImAkVQjaDim9WdapitfzAx6fI7BTiqJOAT915UCEl4XGnwl04wFM+xGdmi+PsXCghhWKz2dz++K/BtuCaOkQF/aja/QqNyiJqwoysjXtSYECFL4K2nPwUiq7d1UUWP7eI546b6RRoHB40+7tac5o7LB4q/pGgAltExbescrPyh3kdbK31ddD0/BRKb5XRPf7xg4t0vtnDoTvtTrrVcBzaQ6637Rjnx28qnlNcxXdWXCeSD7MZODnfFf9ZYnFIIn5wka7r6w1Qa+HQnRqknPCnsal4VL16KISGuYzXwUXqO0+NXTztdD+p4KnRBI6T9ZhoyOHW7kCi06jhuxC26nWksNOi6C5+0x2PTGkVxb0sieILH2ZefB+PB6KfMv2PyQYOwdN6MG2H7tR2Ox0ndMc1/USV89m8OayQsyN3qsrv+0BgmuC9Z0dV9IuXBnsDy/hhlmK3dTgI3WF6n/BCV88wRqHfwRMMK4/y8hL7SxTfx8LvXggXhwRihSrqJq93Ojhy5xApFOjjrx4Ktbt4ZErHxXfBpH/b7NfHr0aWyyO7U8skj1p8duhO5+bmoNOhmO7JuMKhqXge52IlTJTOvOKn0DUGaofuiPyXQegO3Q/d8YyKp5FCqkA+B38gbOjO9tmhABtn5z/OWeXrJX7DaRf2B2ggEErZhbwab/XjAQ7d0XlS/+O8QfdDdy7sPh6lDXdMIFMp5ws4IidW6EYmdQkHnhzfhe70A09kK2flclYRURGL+ZKs5bIL5m9dBbsIrJDth+50v9mhO19GHz/GlUqljL6QtcNAUYVSinOhG1lHTWsndEduXl42WwNFdjUimcv5ZTMzFI88jZ2h5oSgoTv9jeMYw91OsRUqOCSFLw8pzHin8zsSeBJUEDUN3Q4l9/jB3eCgIBdLZVtguZp5JnvUYMKG7owfHGoVQoHCu1Fx1HOqTaa8He7Q0YjXNUfEOwpHJlYYmXh39cjECiMTK4xMrDAyscLIxAojEyuMTKwwMrHCyMQKIxNOITQkRRd/04UdkxJCIaR0VWdZnpdU7XffJTgMwXdL4hXlNkrbYJ5yWeqvTkCFlKLpfX/28zpihXcEnX5iccQEi/9jHlxr/cwIHHfCk6Ihiv0nxsQKhwgTd8Kz5HjcSUzwuBMeB54ECpp4ZgQPmuB1HT/vhGPEXk8c2/n7GeOj0N14tlf8k0gh3Wz2Wr2/LntT23J/7vFRSFXFkdd4xT8OPDGajfSK0Dk8bGw/q7ni+/AryDmiNCxRdlb8881WKpVqtztpimvG+dCBJsixh3pimDfEsMR+Luy2gJBeqbdTNSgYzRle9q8ETbzx5K0dZl2+7Qv3wxi7yGAdh+50Oi06zoYOVLmY8WLpLTK4PAj2kg38CLL1RqpTx6E7EK40xG7r3pSfPQwSKI0EkIkMs946PNpvt9v4eSfM8fAjY2LGKS9Ko2GMqqUq3YPDTvvo6AjlQoF2nncS44e7+yGTIi8p63+c7oLUzc3N6QFlfDqJC3IY+kETX5BCkDo9Oz8z9K8XcXUSBpnk8RYr3bO/KKF3fr51rstft2d9UfOFTEqKpLPsl/Meeb619f27/Oe3eAYlFHbcic6SxuX6Zxx2cvLvL3ExDkeFRA5ZkmW45reLi4u/v/XiPBgS3XlWPGtwpEQ1Gs8kQHG6UIqK558Y6RlFQ0wfUZcUxTWu83/dSeayulyRRAAAAABJRU5ErkJggg==)

## **How load balancing optimizes website and application performance.**
<br/>

 # **What is Load Balancing ?**
- ### **Load balancing lets you evenly distribute network traffic to prevent failure caused by overloading a particular resource. This strategy improves the performance and availability of applications, websites, databases, and other computing resources. It also helps process user requests quickly and accurately.**
<br/>

- ### **Load balancing acts as a “traffic cop,” bringing order to a potentially chaotic situation. In certain environments, such as applications and virtual infrastructures, load balancing also performs health checks to ensure availability and prevent issues that can cause downtime. Load balancing can even provide centralized security across the group of servers that is easier to manage.** 

  #### - Manages traffic spikes and prevents spikes on a single server 
  #### - Minimizes user request response time
  #### - Ensures performance and reliability of computing resources, both physical and virtual
  #### - Adds redundancy and resilience to computing environments
  <br/>

 # **How it Works ?**
  ### **The load balancer uses a predetermined pattern, known as a load balancing algorithm or method. This ensures no one server has to handle more traffic than it can process. Different algorithms manage the process using different techniques. You, therefore, have multiple options to choose from when making a decision on what type of load balancer to use.**

  <br/>

 ### **Here are the basics of how a load balancer works:**

- #### A client, such as an application or browser, receives a request and tries to connect with a server.
- #### A load balancer receives the request,and, based on the preset patterns of the algorithm, it routes the request to one of the servers in a server group (or farm).
- #### The server receives the connection request and responds to the client via the load balancer.
- #### The load balancer receives the response and matches the IP of the client with that of the selected server. It then forwards the packet with the response.
- #### Where applicable, the load balancer handles SSL offload, which is the process of decrypting data using the Security Socket Layer encryption protocol, so that servers don’t have to do it.
- ####  process repeats until the session is over.
<br/>

![this is image](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPUAAADOCAMAAADR0rQ5AAAA4VBMVEX////w8PDIycuHka7V1NH7+/v39/e+xdkAM4ns7Ovi4uDY19X29vZXbaUAMYja3uk3VZdQZ6Le3dhygq9ldaG/wcfp7PMyUJOBjKt6hqmytr9AWpnv8fZbbZ7Pzst6ibLJz9+jqLicpsVRZpuDkblseaAgQ46xuNGSmrF5gpulr8qSnsCAiJ2IjqBneKmdoKhwepmTl6QAAHtebZTP0tkkRY1IXZYMOopvfqUAK4bCxtG9vLjb3eDMz9cADn3S1+W1u82qr76vrahTZJGjpKcAHICWnbAAF3/Ew76Ul56mqrYQStrzAAAR7UlEQVR4nO1dDXuiyNJthMZ0Q4IBFfwCo0ZdnHFn7/Urjrnq3Z3c2fn/P+gFNMpHNXaGSQL75jyZHkVRjk0dqrurCoQ+UBBcY+SY5q2MpqbZkdG1aU5ldGvODRm1zPkAo4o5n2BsmvMebs/NuXts6167606w9xEPrQonTPW96R7R0mVEJYniY6t7LcKJVlYkiRxb9dRu+tT7iIok6TxQlIokvzffA/wDwarq9ZkcaWV2i09tva94G01pZPCgjFt5YW1kORBZDFjrzr52GWOL5Ia1THGGvVWCAtZNSi5BKDdoblhnQ/CL+azJ5fd2rfywvs3S1e2pv3cBWbf0DLTbHR0XknWmi0m7rxST9SCTho8LyvoXaXjBWGdD228KyLqVScM7PosislY+NPyFKKyGT7IcCHYLylomWQxbLqiGZzuMut8UkHUly3G0O/7kSAFZZ9Twgtq1+f9Sw3uZNHxfUNY4m4YLqJCs25n2LqyGA1PUXbOVhCkk3/iP0vDy2LalOBal5O9TWD8c0vCyqKhx4OtSsrMLq+GuBPS1SJNvhFjjXUFZY+B4fdajXhhbmDXCBdXwOrDNY13/100ID3MKs9b8poCsoWVGv693v30J4Y8SyLrdOfb1flO/hPYoR6xbCqRmnl0TGobAYB3YtbKrcmB1lx/WsIZT5E7C6MJ23V4c1jRlqlxeyLVqQm5Y72ANj9r1Dcuu14e+5nFq8c2TmhvWGOAS2PWn30L422ZouK8Kw5rGg1nzMTer9hqwjduu8dZvBwkTfjgof2zrfbVh54R1RYC9lN4gjDLDrgMNx7pyhm/B0tXQx/KrFMRlhJBpoeUXgqHhnl1fncGy64NHGoEsy7h+c2RNsJyTzo2BpeHrz59C+B2263qC9XLhj8+WAethMFaDBnXvjjU05grsOgyGXaPH2N7O3pYUpfRw6Ov/2t4ZL4p5pM3Q8IkTgsGw64OGn1FfSBR7mC58/PlV8J/wXdjeFltgm2/X/4natQ5reDf6fLEutT3Dlknpu4+Z/wSXO8DP9c5gafjj0+cQfmf54ZFRR72zWPT9Xu4fEXR5f1HKXWcz/XAhYtgql4YLkqQkIQFf8c5gafggHBU4HaX54SFgGK929D+LuAr78Fi3uewazXKoVFxgjK9nf4TxF8MPh/YuAsrQNt+uVQ67xtDeRUAFWPvwWTvTMFyWH84xc5RHMDQ8ZtemwuuHFwMsDd/8zmHXCQ0vCiAVPti1EAZs12izKSZrloZPb6/PuO0x/PCMAR7vhhGwzbfrb1ccdi1De+caxwNmafjmx+8h/Jdh10XT8IFzmM2Bpi191pjLD+8US80GA+nAmqXhCfwDrlwe6ePxblgaHqB9Hn6DakY3r3F0r4QzaaaGBxi1VsPZcSOs4QXq6jBpF3j9mbVa0aVS5ShYoIZDe+cUYdJMDfehrST1ZPiwmgECkE84lhTyqFgaHmBYQ6OqdLCBYqtZlHS6hm8W85V9JFboK1eMNCKpGk4USTnKHahmpBhneJw06EmHrtf49HKBNTxJuge8i9tLUSe/6LheFUnSqAIsM27nFSC2sKgaDpAGNRyRZGihByC2sAAaDpGGc3ywDADgVwANB0kjIdNx517DHREinVWF4b3Lzm3l1snBtDGLNKTC29YCADBMaQ+grxpdVe+ateZT/2qX6ZCzwyMNx4RAGl4eJ9VMhyOlgb2v+yVboUQgtNSavuu8GpM0ww9XhDjwLacffmvZ0nF1GJNV/x2XhAzG6e1hkWku5T7Bule1Q9vkzvjdeptl0z4E4BWfdXkUgqsxPFIh/vOoV3bkhxBu3uvaltLTCFRhIFKacPrhPcuOvm0iZozPkG+5MQgdTTppB9gWRM+GA6XvPzMiNBJ7t5pKdIPWynhJb/ftEidCOSzppGEN9+2aRgo3wX2d1PAHOzZDjodAeO5L0O5LCWmFQVqnr7pAGvRIuTMgkhp+ZcdFewhc8V6Cdl8vHxZiHpx0ZcSLZ9aXSKM+zDoaUcnKgGg34qwfXoN192EZ4N9K6jtPrI3JBdJIZZzhuy8cGRBJDR/WYhzbZuJ3eBn884l+PSB99v2Z9WXSLA2PR0qzNDwOZxyz69HKzmrXCsbPSWWp7zyynlw6vT0YwDb+DAgj/vHaPNb7ZlzUXwr+MfyRtUwvx2O34DOcLwMC8MOvo87YqBoX9ZfixawRNBMQA0vDd+EEiC9/M67XySMSbtahZ9tlKatv5n1Hu6sh72+7RVo35dPOGn4RU5YfzpMBoVpJTrOb/enxblmzM7pmwZVr3pkNTfLwjVT+s2ZfvV7AmqXh0QyILsMPV4HTd9NadIMH3Va/BEy2vRBeX2ud30rVe9v486vY/4ttMC9gzfbDOTIgkoc4ur4WhH3/yrw1l529bd+2elBO5AvgsUaCQlSFyooi62mRuPysrxlXrvWnT5czmw4V/E7QpstG03roCUQpNWu2pIyXTz/ur267nMcCom3elTnBzxqq4PeCjMWQfdRvW17nKqTUeZi65fWuN71alWxKlLt5C8qf4gSeNCxOPHG7BiwNnzghs3ZYmU0hDd8uPc5U9WdQ7Kf7fr/TuCvZiuBnSdC7h3XoeKa8I8fJYSfKDe6LpMPjh7Ps+lCF9YDB3n7ueCxQRZIkhT5LGe7dh5wXWeEcOdr+TuoVdxVj/lk6qILfIbMpBFZmU1jDB08RrywaCz+6D3to6uVR4wE+i3bHfk4wgBZkQrCzxXcCGRCMzKbQt8RYRxFl/SL44+ttkOi5rUNLMmcI/GoGVWE9ZECEM5tYGRCd896vx1ov/+/mwccq/dqJ+fuaoeF8mU2HyiEHvB5rZXtYhqh8kVLf+QLWLA2PZEB8ZkVKhzT8FVl7V4UgP+x7+hTckfWIo9wPVIWVO7PpUIX1+EGvyBql5QptJ4bj+kyPrLXh5dA/loZzZUCkaHgUWVmzoVVW+1pzbBry6QyfLX8q4hHIgGBkNoVOpvdh3TVnnjv4SOjdnJzs+jJtqAorfwZE52wer8i63uvi3gi5Lir3omO49lyRBFQ31xreLchJzS7SZvnhmHDY9dto+HF8TR++6ZX/RWcjp2upPEK99dO/DNnYn8+KS7RZGp7AO2p43fpcEj/ZvS9f3S+R8TVuSQS5y6ubZuk71fr9s5cyS5c0qArrmTU+D5bALLa7t2Cte5IrYEr8PxIR321D6bqT+aLVdJ06MhehMVc6bagK64l12ayazzuDaia/gYYPrR4Lhqhff7tp/LH0uttSzS/hkWYqbWiY8sxaNnWp1kqJlA7Xjnot1mj3dMeCda+jpnmzHC7//Os7GX6P8EyjDVVhDUVKC8iU0qJnz9ryaqxl9t00NkOl4urjZevvvxY9rRNbZUmhDVbwO0VKl1C5kxYp3Xl9u07FdP9oztf61/sby+7s41TYtFMjpWem2UmNlH4DDU9FfamQp6vbZbVm3/WT6w3MCxhUhTUUKS2lRkofKvgdMLh7B9ZovZxhKv6w9Z5pA2ctizZUhTV0vZZPK2qwhp+3De5SZq1ejTV6nE93s13PXEng2imDNjQsK4sAAXYFvwMmnrvOxFXjtVhjsm/c349LtgKvN8C0wSqsQzM5Fze8oOFIsVNwYWicBSrRFYUCBVAOAGmDlbQpFCkNnEGRmGGcqGEaqWf6k5y4kPrhEG2wkjZIAHhfEeLDwQsYVIWVG2ENzzGStKEqrPyA6v/lEAna0Iqjdg0B0sgM61dvijhtSMPLIrD60gIjpYvR1wlJg/3wZKS0XOjcvTjtTFnn9QJVDonQZlRhRdvIenid4ZtB9f/yik2INqMKK2ekNH6PUP+fDfkI0WZVYZ38O4xPcOQVhur//XqUIweJDWHqDXZO2/iP4XySsyr4YRqpR8eKlH4TDR+tBST3yqjeG3lu1WhFhzV12CSui9Ret3vbxd5r2zLHVfTU26wKft1wBsSIkQHxRh7pqEnQdCPuNLKaOev9vdKw3MmPDentnNnjrFoyqDjr7Dcc1raZH5wyhobz2fUbVX8KWOubxm4wbV7rm3tFbDT2zUfHGW+qk81Kb4libcpKYorimG7OqsLq3oftmpEB8UYV/Ny7De6Uers+XTWrtfJKsXbjp1p1I4pr0plVqdF8pB3G+DqO4/EyqrBingyIN9JwbSyuqbVX1tau5v3vPaD0cbaxds31pKm4d0QckzIn6wO6wLYgs8kNg2HXb1XBT6BEEIKVDyKr3mNZxViVvS2qQAWZCN42zJxWgMCoHMJ3D4jCVX96BkvDI5lNX1iZTYWYVQDA9MMjdq2/m4ZHr8JdzyXRuodt9QzF1RhVWDnv7QLVjvq1mPoW6H9J8EXlvYBu12LPf1IXyc8nj8PVn3gjpV9dww1Pm52Bg9yJgybO9Z135ZZ2483AKGsWHTi7+nRgoN5gUnec2chwOWMqWVVYue7t8gYabijytkfFteAuZlN69+SZmrgSyXZy/ygSbVx9FPWOMqXU2O1XvabCqa7M6k98kdKvruGGgjWR9JpTUq1V6cjva530971NtSbuerRTE+lUmupo0Cwp4ybvrDtTw7kym15fw517p75yHP3a6khOY+qztpxdsyo2Sp311GrURGLok9Wk3rceXZ6bKAXg8sNfcA+IXw3sr2sE/3TF/+cpmEIpoYruNUFLZKpuFEK891DeISCrgl/s3i4MPzxWhbXtDABku6GhrMqe44uRX7AFB0soh0eyH2wYtOjwgu+wcX8qq4Ifj13HAzy0ajOJfjN/E6mMKqx893aJV2HVVlIQnPaMILLc4La2twOXH87OWIwS0la63z7fmLISzHBx3ajtjcGq4Be7twufH66tgkuHfLp5j/9qHlkz/XCee7skWdeCgenJk/Ue1/PImqXhk7BZOztWFdbotI3W6MSDw6xGHu2a5Yfz3QMi5v57rGtKNMNqvarmkDWrCivXvV3iVVh91rF3bRt5ZM3ywzFnxmLkOcBayyXr1CqsYXCpWVFYX1jTPHuT/yjWqVVYu/N+5XktjacKa2FYp1VhxXNdai7SIqUBDS8E67QqrIdI6ZTo2XgV1sKwZlZ/QkHlJrRNj5Qu6BmequGP81a/xBkp7eOfwRrRUmqkdLtaUNZQFdbQlUtNj5QWCnqGw5VDeCOli6rhUB3V7jJZUrq1hM7w2N6FYQ1puGfOUIz35SqshWENVtKGQ72T7yushkNVWLnRXhWUtZrpvmL/JA3/aRSGNVSFlRvxKqyFYQ1qOC8Kq+HgfQF4UVgN72RhrTYKyhqq4MePDw1HBWI9zbJzrIJfcVhDVVi58aHhPgrDGqrCyg1VTLC23Eg4/WiSS9bZNDxWhVVb1cZx1PLI+pdCW+nH5TFCn9fJSB5XcqEqrNxIaHgQoeHDeJg8v5THVXuo+hM3Ehr+zLr9bfjwnB6XR9a/VsNPfX398OfX4+RLHllDVVi5Ea7g5+MYjeOj9PX5B8gja6iCHz/iGr7snIr+nR4Nc8g6G+LnyaYGgOs2Cm+LpV86uW44TtdvjS3SvFZD26Dtem392Ja9VkA7x3FUNPJaGbmOEa/si8HCufnLDak3myWCZrVmScCz5qmVH5tNW/VbSVW9VlHbfisf27XXYq8t5f2+ewz4d3KXg2pahxZHWpUQAWoFv/IwCnb9wAc+8IEPfOADHygKMt7C5t0g/3QKlmsY8hQ7p/FGPfc/gYPK5cO92bpukPtrGFDMfBq2IlXoVF+Teq+LtqMRGjj1ci9bGtsrw6CORa5puSdvrYmGUNnVFbKd1Ntbt+sn77ojtC1vU0n01lRGU31KpqRBrzfVmftUMzabPHvrbs2wmuLe3Tjrlb0gCHccok1odbZQxuutO9jsd447m9IUEu6eII+1oSwsp+Tok5q7p25nlr+h5hlaQ3yy9lbDGu/G1CipSK4tdlXLqo0V0mjQhWjtnRIdpZFot2blttfXeqf2SA3qsX7aaGsrf9MKZ+CrH7OH0q5hz3bVza2klrv1fq1qP679W0JUdWc/2xglj0QjhcTGuqPr2lqhokvWdDsjk7U7lnI9Ch99J66t7qzHza5RUmTkijNlba03a4q0piKPRdpVhFE6iaCsiSpgbxyOVSRgmeR9FI4JDv6IqvrZekgmVMXePz/AThD8iQZZ8F/ONYm3wf8BVV2S9P1ZfbUAAAAASUVORK5CYII=)
<br/>


# **Top 5 Reasons of using Load Balancers**

 ## **1. Your company is growing and you're adding more seats**
 <br/>

  - ### **A potential problem is lurking that you must address: network server overload. How are you going to solve this problem? You can't afford for your users and external clients to be denied access to the applications they rely on to run your business or to conduct business with you. You have options: you could use freeware load balancers, software load balancers, dedicated hardware, or virtual load balancers.**
  <br/>

- ### **A load balancer solution allows you to plan for handling increased traffic by inserting devices into your network that can ensure that the traffic is directed to servers that are performing and have spare capacity. This "traffic cop" feature is called Application Distribution Control (ADC), and as its name suggests, it is the most intelligent way to keep your servers up and running and your internal and external clients happy.**
<br/>

## **2. The number of network users isn't growing but traffic is**
<br/>

- ### **Your CFO will be delighted if this traffic is being generated by hungry clients looking to buy more products and services from your Web site, but less so if this increase is caused by a greater internal load. However, your CFO can remain content that if there is more internal and/or external traffic, the business is running briskly and demand for all that the company can supply is increasing.**
<br/>

- ### **What is the problem? You need to analyze server loads to examine what is happening and when. Different servers will be affected with higher traffic volumes at different times of the day. For this reason, load balancers are essential to balancing the traffic and making sure that new sessions are brought to servers with adequate spare capacity so that overloaded servers can process their backlog successfully and ten be returned to the pool of resources available for new sessions.**
<br/>

- ### **Consider a typical business day. Each morning the staff arrives and accesses e-mail servers by sending and receiving messages, thus generating e-mail traffic. As the morning progresses video conferences begin and the unified communications (UC) traffic increases. Then clients and suppliers access the portals and the e-commerce sites in increasing numbers. This process repeats itself in the afternoon until the business day ends and the traffic volume declines. If you are using load balancers, traffic is directed smoothly to the servers that are operating as expected.**
<br/>

## **3. You need to balancing load across globally-distributed servers -- that is, perform global server load balancing (GSLB)**
<br/>

- ### **Having invested in a globally distributed server infrastructure, it is important to make sure that these servers can provide a satisfactory provisioning service for all users. For this reason, you should look out for "Geo" solutions from the load balancer suppliers. You will find that there are a certain number of vendors who can offer you a solution; however, you need to choose carefully.**
<br/>

- ### **Some manufacturers simply will offer you a local load balancer that can be configured to act as a "Geo," while others have developed a specific load balancer that is optimized to act as a "Geo." There are key differences: price and functionality.**
<br/>

- ### **One vendor's approach might be to take a standard local load balancer with all of the functionality it needs to perform locally and add "Geo" functionality. Another vendor may take a completely different approach and design a "Geo" to do its specific job -- that is, to direct traffic to local load balancers alone. Not surprisingly. the latter solution is very cost effective because it's the right tool for the job.**
<br/>

## **4. You cannot decide if you need to virtualize your environment**
<br/>

- ### **Once you have configured virtual machine applications on a single server, what happens if the server goes down? You may not have a problem of you have set up a second server in High Availability (HA) mode. However, if you have users or clients working with the primary server and it fails, all of their input (such as sales orders) could be lost because the servers alone cannot seamlessly pass the session from one server to another in HA mode for all applications.**
<br/>

- ### **Once you have configured virtual machine applications on a single server, what happens if the server goes down? You may not have a problem of you have set up a second server in High Availability (HA) mode. However, if you have users or clients working with the primary server and it fails, all of their input (such as sales orders) could be lost because the servers alone cannot seamlessly pass the session from one server to another in HA mode for all applications.**
<br/>

- ### **The solution is to implement a HA load-balancing solution.. Make sure to choose a Virtual Load Balancer solution that is available for VMware as well as Microsoft Hyper-V and with a GUI and management consoles that are the same for both.e solution is to implement a HA load-balancing solution.. Make sure to choose a Virtual Load Balancer solution that is available for VMware as well as Microsoft Hyper-V and with a GUI and management consoles that are the same for both.**
<br/>

## **5. You need high availability without the high cost**
<br/>

- ### **Enterprises are moving toward unified communications environmenst in which e-mail, voice, documents, and instant messaging can all be integrated. UC appliances are needed that support Microsoft Lync high availability. "Uh huh for failover, right?" Yes. If one server fails, traffic needs to be redirected to the best operating one. In this example, a payout of $10,000 or more minimum for all licenses.**
<br/>

- ### **With a load balancer that has "stateful failover" built in , there is no need for those expensive Microsoft SQL Server licenses. Better still, such a load balancer is available in an HA configuration either in the virtual or hardware form factor.**
<br/>

![this is image](https://www.nginx.com/wp-content/uploads/2014/07/what-is-load-balancing-diagram-NGINX-640x324.png)
<br/>
<br/>

# **Load Balancers helps to Resolve Scalability issues**
<br/>

* ### **If you have a website, you must be uploading engaging content to attract readers. And it must be exciting to see a growing number of visitors on your site. However, it is important to remember that the amount of traffic on the website has a direct effect on the performance of the website. If there is a sudden spike in the traffic, it might become difficult for your server to handle the excess traffic and the website may crash. By introducing load balancing, the traffic can be spread across multiple servers and the increase in the traffic can be handled in a much easier manner. Depending on how the site’s traffic fluctuates, the server administrators can scale the web servers up or down depending upon your site’s needs.**
<br/>

# **Load Balancers to resolve Performance Issues**
<br/>

* ### **If your company is located in just one place, then you can schedule the maintenance and planned downtime at non-working hours like early mornings or the weekends. However, if you have a global business with offices scattered across the world, with different time-zones, you need to implement load balancing. This will enable you to shut off any server for maintenance and channel traffic to your other resources without disrupting work in any location. This way you can reduce the downtime, maintain the uptime and improve the performance.**
<br/>
<br/>

# **Horizontal Vs. Vertical Scaling: How Do They Compare?**
<br/>

## **What Is Scalability?**
<br/>

- Scalability describes a system’s elasticity. While we often use it to refer to a system’s ability to grow, it is not exclusive to this definition. We can scale down, scale up, and scale out accordingly.

- If you are running a website, web service, or Scalability describes a system’s elasticity. While we often use it to refer to a system’s ability to grow, it is not exclusive to this definition. We can scale down, scale up, and scale out accordingly. 

- If you are running a website, web service, or application, its success hinges on the amount of network traffic it receives. It is common to underestimate just how much traffic your system will incur, especially in the early stages. This could result in a crashed server and/or a decline in your service quality. 
 
- Thus, scalability describes your system’s ability to adapt to change and demand. Good scalability protects you from future downtime and ensures the quality of your service.
<br/>

## **What Is Horizontal Scaling?**
<br/>

- Horizontal scaling (aka scaling out) refers to adding additional nodes or machines to your infrastructure to cope with new demands. If you are hosting an application on a server and find that it no longer has the capacity or capabilities to handle traffic, adding a server may be your solution.

- It is quite similar to delegating workload among several employees instead of one. However, the downside of this may be the added complexity of your operation. You must decide which machine does what and how your new machines work with your old machines. 

- You can consider this the opposite of vertical scaling.
<br/>

## **What Is Vertical Scaling?**
<br/>

- Vertical scaling (aka scaling up) describes adding additional resources to a system so that it meets demand. How is this different from horizontal scaling? 

- While horizontal scaling refers to adding additional nodes, vertical scaling describes adding more power to your current machines. For instance, if your server requires more processing power, vertical scaling would mean upgrading the CPUs. You can also vertically scale the memory, storage, or network speed.

- Additionally, vertical scaling may also describe replacing a server entirely or moving a server’s workload to an upgraded one.
<br/>
<br>

![this is an image](https://www.cloudzero.com/hubfs/blog/horizontal-vs-vertical-scaling.webp)
# *Horizontal V/S Vertical Scaling*
<br/>

## **Advantages of Horizontal scaling**
<br/>

- Scaling is easier from a hardware perspective All horizontal scaling requires you to do is add additional machines to your current pool. It eliminates the need to analyze which system specifications you need to upgrade.

- Fewer periods of downtime - Because you’re adding a machine, you don’t have to switch the old machine off while scaling. If done effectively, there may never be a need for downtime and clients are less likely to be impacted.

- Increased resilience and fault tolerance - Relying on a single node for all your data and operations puts you at a high risk of losing it all when it fails. Distributing it among several nodes saves you from losing it all. nd clients are less likely to be impacted.

- Increased performance - If you are using horizontal scaling to manage your network traffic, it allows for more endpoints for connections, considering that the load will be delegated among multiple machines.     
<br/>

## **Disadvantages of horizontal scaling**
<br/>

- Increased complexity of maintenance and operation - Multiple servers are harder to maintain than a single server is. Additionally, you will need to add software for load balancing and possibly virtualization. Backing up your machines may also become a little more complex. You will need to ensure that nodes synchronize and communicate effectively.

- Increased Initial costs - Adding new servers is far more expensive than upgrading old ones.
<br/>

## **Advantages of vertical scaling**
<br/>

- Cost-effective - Upgrading a pre-existing server costs less than purchasing a new one. Additionally, you are less likely to add new backup and virtualization software when scaling vertically. Maintenance costs may potentially remain the same too.

- Less complex process communication - When a single node handles all the layers of your services, it will not have to synchronize and communicate with other machines to work. This may result in faster responses.

- Less complicated maintenance - Not only is maintenance cheaper but it is less complex because of the number of nodes you will need to manage. 

- Less need for software changes - You are less likely to change how the software on a server works or how it is implemented. 
<br/>

## **Disadvantages of vertical scaling**
<br/>

- Higher possibility for downtime - Unless you have a backup server that can handle.
operations and requests, you will need some considerable downtime to upgrade your machine. 

- Single point of failure - Having all your operations on a single server increases the risk of losing all your data if a hardware or software failure was to occur. 

- Upgrade limitations - There is a limitation to how much you can upgrade a machine. Every machine has its threshold for RAM, storage, and processing power. 
<br/>
<br/>


### **Refrence links :-**
<br/>

[Vertical and Horizontal Scaling ](https://www.cloudzero.com/blog/horizontal-vs-vertical-scaling#:~:text=Cost%20%2D%20Initial%20hardware%20costs%20for,the%20best%20option%20for%20you.)

[Load Balancers](https://www.ibm.com/cloud/learn/load-balancing#:~:text=Load%20balancing%20lets%20you%20evenly,user%20requests%20quickly%20and%20accurately.)

